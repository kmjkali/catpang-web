import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)


import CardItemScratchInfo from '~/components/card-item-scratch-info'
Vue.component('CardItemScratchInfo', CardItemScratchInfo)
